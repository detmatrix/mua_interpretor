package mua_interpretor;

public class Error extends Value{
	public Error() {
		this.setType("Error");
		this.setVal("Error");
	}
	public Error(String msg) {
		this.setType("Error");
		this.setVal(msg);
	}
}
