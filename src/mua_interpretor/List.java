package mua_interpretor;

import java.util.ArrayList;

public class List extends Value{
	private ArrayList<Value> lst;
	
	
	public List(ArrayList<Value> content){
		this.setType("list");
		lst = content;
		String val = "";
		for (int i = 0; i < lst.size(); i++) {
			Value item;
			String type;
			item = lst.get(i);
			type = item.getType();
			if (type.equals("list")) {
				val = val.concat("[");
				val = val.concat(item.getVal());
				val = val.concat("]");
			}
			else
				val = val.concat(item.getVal());
			if (i < lst.size() - 1)
				val = val.concat(" ");
		}
		
		// brackets are not needed in the value 
		this.setVal(val);
	}
	
	public String[] getListContent(){
	    return this.getVal().split("\\s+");	   
	}
	
	public ArrayList<Value> getContent(){
		return lst;
	}
}
