package mua_interpretor;

public class Word extends Value{
	public Word(String val){
		this.setType("word");
		this.setVal(val);
	}
	public Boolean isNumber() {
		if (!Character.isDigit(this.getVal().charAt(0)) &&
				this.getVal().charAt(0) != '-')
			return false;
		try{
			Double.parseDouble(this.getVal());
		}
		catch(NumberFormatException nfe){
			return false;
		} 	
		return true;
	}
	public Boolean isBool() {
		String val = this.getVal();
		return val.equals("true") || val.equals("false");
	}
}
