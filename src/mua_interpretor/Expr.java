package mua_interpretor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Scanner;

public class Expr {
	private  int idx = 0;
	private int idx_in_string = 0;
	private static Hashtable<String, Value> symTab;  // symbol table
	private static Scanner sc;  // input scanner, opened in the Interpreter
	
	private static String[] arthimetic_op= {"add", "sub", "mul", "div", "mod"};
	private static String[] cmp_op = {"eq", "gt", "lt"};
	private static String[] binary_logic_op = {"and", "or"};
	private static String[] unary_logic_op = {"not"};
 	private String[] line; // one line of input command, passed from the Interpreter
	
	public Expr(String[] input_expr){
		line = input_expr;
	}
	public Expr(String[] input_expr, Hashtable<String, Value>sym_tab, Scanner global_sc){
		// this constructor should only be used to initialize static variables for the class
		line = input_expr;  
	    symTab = sym_tab;
	    sc = global_sc;
	}
	
	public void print(){
		System.out.println(line);
	}
	public int getIdx() {
		return idx;
	}
	public int getLineLength() {
		return line.length;
	}
	
	
	public Value parse(){
		if (idx >= line.length)  // check the index, stop if index out of bound because of error in the input line 
			return new Error("can not parse, index out of bound!");
		
		if (line[idx].charAt(0) == '\"'){  // start w/ a '"', take as a word
			if (line[idx].length() <= 1)
				return new Error("no word after \" !");
			Word new_word = new Word(line[idx++].substring(1));
			if (new_word.isNumber())
				return new Number(new_word.getVal());
			else 
				return new_word;
		}
		else if (Character.isDigit(line[idx].charAt(0)) ||
				line[idx].charAt(0) == '-'){  // start w/ a digit or a minus sign, try to parse it as a double
			try{
				Double.parseDouble(line[idx]);
			}
			catch(NumberFormatException nfe){
				return new Error("Error in the number");
			}
			return new Number(line[idx++]);			
		}
		
		else if (line[idx].charAt(0) == '[') { // start w/ '[', try to get a list(eat several strings in the input)
			Value lst = this.getList();
			idx++;
			return lst;  // return either a list or Error, two cases are merged here
		}
		
		else if (line[idx].equals("make")){ // make [word] [value]
			idx++;
			Value word = this.getWord();
			if (word.getType().equals("Error")) // if get errors when parse the word or value,
				return word;                    // then just return the error
			Value val = this.getAValue();
			if (val.getType().equals("Error"))
				return val;
			symTab.put(word.getVal(), val);  // put the binding to the symbol table 
			return new None();  // normally nothing is returned
		}
		
		else if (line[idx].equals("thing")){  // thing [word]
			idx++;
			Value word = this.getWord();
			if (word.getType().equals("Error")) // if get errors when parse the word, just return the error
				return word; 
			String key = word.getVal();
			if (symTab.containsKey(key))
				return symTab.get(key); // get the binding from the symbol table
			else
				return new Error("this word is not defined!"); 
		}
		
		else if (line[idx].charAt(0) == ':') {  // :[word], have no space in between
			if (line[idx].length() < 2)
				return new Error("no key after ':' !");
			String key = line[idx].substring(1);  // get the word without parsing
			idx++;
			if (symTab.containsKey(key))
				return symTab.get(key); // get the binding from the symbol table
			else
				return new Error("this word is not defined!");
		}
		
		else if (line[idx].equals("erase")) { // erase [word]
			idx++;
			Value word = this.getWord();
			if (word.getType().equals("Error"))
				return word; 
			if (!symTab.containsKey(word.getVal()))
				return new Error("can not erase, this word is not defined!");
			else 
				symTab.remove(word.getVal());  // remove the binding from the symbol table
			return new None();
		}
		
		else if (line[idx].equals("isname")) { // isname [word]
			idx++;
			Boolean isIn; 
			Value word = this.getWord();
			if (word.getType().equals("Error"))
				return word;  
			else 
				isIn = symTab.containsKey(word.getVal()); 
			return new Bool(isIn.toString());  // change from a native Boolean to a Bool class
		}
		
		else if(Arrays.asList(Expr.arthimetic_op).contains(line[idx])){ // op [number] [number]
			String op = line[idx];
			idx++;
			Value num1 = this.getNumber();
			if (num1.getType().equals("Error"))
				return num1;
			Value num2 = this.getNumber();
			if (num2.getType().equals("Error"))
				return num2;
			double v1 = Double.parseDouble(num1.getVal());
			double v2 = Double.parseDouble(num2.getVal());
			Double res = 0.0;
			switch (op) {       // do something with two numbers according to op
				case "add": res = v1 + v2; break;
				case "sub": res = v1 - v2; break;
				case "mul": res = v1 * v2; break;
				case "div": res = v1 / v2; break;
				case "mod": res = v1 % v2; break;
				default: return new Error("no such opeartion!");
			}
			return new Number(res.toString());  // store the result back as a Number class
		}
		
		else if (Arrays.asList(Expr.binary_logic_op).contains(line[idx])) { // op [bool] [bool]
			String op = line[idx];
			idx++;
			Value bool1 = this.getBool();
			if (bool1.getType().equals("Error"))
				return bool1;
			Value bool2 = this.getBool();
			if (bool2.getType().equals("Error")) 
				return bool2;
			Boolean v1 = Boolean.parseBoolean(bool1.getVal());
			Boolean v2 = Boolean.parseBoolean(bool2.getVal());
			Boolean res = false;
			switch (op) {  // do something with two numbers according to op
			case "and": res = v1 & v2; break;
			case "or": res = v1 | v2; break;
			default: return new Error("no such opeartion!"); 
			}
			return new Bool(res.toString()); // store the result back as a Bool class
		}
		
		else if (Arrays.asList(Expr.unary_logic_op).contains(line[idx])) { // op [Bool]
			String op = line[idx];
			idx++;
			Value bool1 = this.getBool();
			if (bool1.getType().equals("Error"))
				return bool1;
			Boolean v1 = Boolean.parseBoolean(bool1.getVal());
			Boolean res = false;
			switch (op) {
			case "not": res = !v1; break;
			default: return new Error("no such opeartion!");
			}
			return new Bool(res.toString());
		}
		
		else if (Arrays.asList(Expr.cmp_op).contains(line[idx])) { // op [word] [word], compare their ASCII code
			String op = line[idx];
			idx++;
			// get two words or values
			Value w1 = this.getAValue();
			if (w1.getType().equals("Error"))
				return w1;
			Value w2 = this.getAValue();
			if (w2.getType().equals("Error"))
				return w2;
			String v1, v2;
			v1 = w1.getVal();
			v2 = w2.getVal();
			
			Boolean res = false;
			int tmp = v1.compareTo(v2);  // tmp = v1 - v2
			switch (op) {
			case "eq": res = (tmp == 0); break;
			case "lt": res = (tmp < 0); break;
			case "gt": res = (tmp > 0); break;
			default: return new Error("no such opeartion!");
			}
			return new Bool(res.toString());
		}
		
		else if (line[idx].equals("print")){ // print [value]
			idx++;
			Value val = this.getAValue();
			if (val.getType().equals("Error"))
				return val;
			System.out.println(val.getVal());
			return new None();
		}
		
		else if (line[idx].equals("read")) { 
			// read a string, if it begins with a digit or minus sign, treat it as a number
			// else treat it as a word('"' is not needed in the word!)
			idx++;
			String newVal = null;
			newVal = sc.nextLine();
			if (newVal == null) {
				return new Error("unsucessful read");
			}
			else if (Character.isDigit(newVal.charAt(0)) ||
					newVal.charAt(0) == '-'){
				try{
					Double.parseDouble(newVal);
				}
				catch(NumberFormatException nfe){
					return new Error("Error in the number");
				}
				return new Number(newVal);			
			}
			else
				return new Word(newVal);  // don't need " here, take everything else as a word			
		}
		
		else if (line[idx].equals("readlinst")) { // read an array of string and feed them into a list
			idx++;
			String input;
			input = sc.nextLine();
			String lst = "[";    // add brackets to the input
			lst = lst.concat(input);
			lst = lst.concat("]");   
			Expr exp = new Expr(lst.split("\\s+"));
			Value ret_val = exp.parse();    // make a new expression using the input list
			if (!ret_val.getType().equals("list"))  // and try to parse it
				return new Error("can not read a list");
			return ret_val; 
		}
		
		else if (line[idx].length() >= 2 && line[idx].substring(0, 2).equals("//")) {  
			// a line begins with '//' is comment, simply ignore it
			return new None();
		}

		else{ 
			String func = line[idx++];
			if (symTab.containsKey(func)) {  // it is a function name
				List lst = (List)symTab.get(func);
				List paras = (List)(lst.getContent().get(0));
				List body = (List)(lst.getContent().get(1));
				List args = (List)this.getList();
				Function fun = new Function(paras, body);
				Value retval = fun.call(args);
				new Expr(new String[0], symTab, sc);
				return retval;
			}
			else
			  return new Error("invaild expression!!");
		}
	}
	
	// just parse the line to get a value 
	public Value getAValue() {
		Value val = this.parse();
		return val;
	}
	
	// parse the line and get a value, verify its type 
	public Value getWord() {
		Value word = this.parse();   
		if (!word.getType().equals("word")) {
			return new Error("error parsing as a word");
		}
		else 
			return word;
	}
	
	public Value getNumber() {
		Value num = this.parse();
		String type = num.getType();
		if (type.equals("number")) {
			return num;
		}
		else if (type.equals("word")) {
			if (((Word)num).isNumber())
				return new Number(num.getVal());
			else
				return new Error("error parsing as a number");
		}
		else
			return new Error("error parsing as a number");		
	}
	
	public Value getBool() {
		Value bool = this.parse();
		String type = bool.getType();
		if (type.equals("bool")) {
			return bool;			
		}
		else if (type.equals("word")) {
			if (((Word)bool).isBool())
				return new Number(bool.getVal());
			else
				return new Error("error parsing as a bool");
		}
		else 
			return new Error("error parsing as a bool");
	}
	
	Value getContent(ArrayList<Value> content) {
		if (idx >= line.length)
			return new Error("can not parse the list, index out of bound!");
		if (idx_in_string >= line[idx].length()) {
			idx++;
			idx_in_string = 0;
		}
		if (idx >= line.length)
			return new Error("can not parse the list, index out of bound!");
		else if (line[idx].charAt(idx_in_string) == '[') {
			// idx_in_string++;
			Value sub_lst = getList();
			if (sub_lst.getType().equals("Error"))
				return sub_lst;
			content.add(sub_lst);
			return new Bool("false");
		}
		/*else if (line[idx].charAt(line[idx].length()-1) == ']') {
			content.add(new Word(line[idx].substring(idx_in_string, line[idx].length()-1))); // get rid of last ']'
			idx_in_string++;
			return new Bool("true");
		}*/
		else if (line[idx].endsWith("]")) {  // there may be many ']' in a string
			int new_idx = line[idx].indexOf(']', idx_in_string); // search for the next ']'
			if (new_idx > idx_in_string)  // when there is some content between two ']'
				content.add(new Word(line[idx].substring(idx_in_string, new_idx)));
			idx_in_string = new_idx + 1;
			return new Bool("true");
		}
		else {
			content.add(new Word(line[idx].substring(idx_in_string)));
			idx++;
			idx_in_string = 0;
			return new Bool("false");
		}
		
	}
	
	Value getList() {
		ArrayList<Value> content = new ArrayList<Value>();  // put the content in the list(so we can append it)
		Value list_end;
		idx_in_string++;
		do{
			list_end = getContent(content);
			if (list_end.getType().equals("Error"))
				return list_end;  
		}while (list_end.getVal().equals("false")); 
		// if the brackets of a list is not closed, then 
		// the index will out of bound and an error is returned
		
		return new List(content); 
	}
	
	public static void main(String[] args) {
		Hashtable <String, Value>tb = new Hashtable<String, Value>();
		//String[] arg1 = {"make", "\"a", "[12", "[[[23", "23]]]", "[35]", "36]"};  // a nested list 
		String[] arg1 = {"make", "\"fun", "[[a", "b]", "[print", "add", ":a", ":b]]"}; 
		//String[] arg2 = {"thing", "\"b"};
		String[] arg2 = {"fun", "[2","3]"};
		Scanner sc = new Scanner(System.in);
		Expr exp = new Expr(arg1, tb, sc);
		exp.parse();
		exp = new Expr(arg2);
		Value val = exp.parse();
		System.out.println(val.getVal());
	}
}
