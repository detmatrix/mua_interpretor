package mua_interpretor;

import java.util.Hashtable;
import java.util.Scanner;

public class Interpreter {
	static Hashtable <String, Value>sym_tab = new Hashtable<String, Value>(); // symbol table
	public static void evaluate_and_check(Expr exp) {
		Value ret_val = exp.parse(); // get return value and type
		String ret_type = ret_val.getType();
		
		if (ret_type.equals("Error")) {  // an error is returned, print its error message
			System.out.println(ret_val.getVal());
		}
		
		else if (exp.getIdx() != exp.getLineLength()) {  // something can not be parsed, the expression is invalid
			System.out.println("invalid expression!");
		}
		
		// check the return type, normally a None is returned
		else if (ret_type.equals("list")){ // a list is returned, try to parse it as a new input
			Expr sub_expr = new Expr(((List)ret_val).getListContent());
			evaluate_and_check(sub_expr);
		}
		
		else if (ret_type.equals("word") || ret_type.equals("number")){ 
			// word and number can not be returned directly
			System.out.println("Error, can not evaluate a word or a number");
		}
		return;
	}
	public static void main(String[] args){
		String input;
		Scanner sc = new Scanner(System.in);
		new Expr(new String[0], sym_tab, sc); // initialize Expr class w/ symbol table and scanner
		while (true){
			input = sc.nextLine();
			String []line = input.split("\\s+");  // keep reading a line and splitting it w/ space
			if (line[0].equals("exit"))  // jump out of the loop if exit
				break;
			
			Expr expression = new Expr(line);  // create a expression and parse it		
			evaluate_and_check(expression);
		}
		sc.close();
		System.out.println("goodbye!");
	}

}
