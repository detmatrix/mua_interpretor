package mua_interpretor;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

public class Function {
  private static Hashtable <String, Value>localSymTab;
  private List funcBody;
  private List funcArgs;
  Function(List paras, List body){
	  localSymTab = new Hashtable <String, Value>();
	  funcArgs = paras;
	  funcBody = body;
  }
  
  Value call(List args) {
	  ArrayList<Value>ParaList = funcArgs.getContent();
	  ArrayList<Value>ArgList = args.getContent();
	  //ArrayList<Value>CommandList = funcBody.getContent();
	  int argc = ArgList.size();
	  assert(argc == ParaList.size());
	  for (int i = 0; i < argc; i++) {  // bind name to value in local symbol table
		  assert(ParaList.get(i).getType().equals("word"));
		  localSymTab.put(ParaList.get(i).getVal(), ArgList.get(i));
	  }
	  Scanner sc = new Scanner(System.in);
	  // use FuncExpr, add support for special function operations
	  new Expr(new String[0], localSymTab, sc); // change the symbol table to the local one
	  Value retVal = new None();
	  //for (int j = 0; j < CommandList.size(); j++) {
	//	  assert(CommandList.get(j).getType().equals("list"));
	//	  Expr exp = new Expr(((List)CommandList.get(j)).getListContent());
	//	  retVal = exp.parse();
	//  }
	  Expr exp = new Expr(funcBody.getListContent());
	  retVal = exp.parse();
	  return retVal;
  }
}
